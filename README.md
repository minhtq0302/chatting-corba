# Chatting Corba 
Nhóm DHCNTT16BVL
1. Trần Quang Minh
2. Huỳnh Ngọc Tài
3. Phạm Minh Thành
4. Nguyễn Hoàng Giang
5. Lê Thị Diễm Phương

## Giới thiệu

- Ứng dụng Chat đa người dùng bằng ngôn ngữ Java, giao diện Swing với MultiThreading, ThreadPool để tăng tốc độ xử lý đồng thời kết hợp với Synchronized đồng bộ tin nhắn theo trình tự thời gian và được lưu trữ với CSDL MySQL. 
- Ứng dụng tự động khởi tạo schemas trong CSDL MySQL


	orb_chatter

và 2 bảng 


	Chatter: lưu trữ thông tin người dùng hệ thống
	Message: lưu trữ các tin nhắn của người dùng trong hệ thống

Có thể thay đổi tên schemas và tables tùy ý
- Sử dụng IDL có thể biên dịch ra nhiều ngôn ngữ khác nhau (Java, C++...)

- Kiến trúc:


	Kiến trúc Corba
	ThreadPool, MultiThreading
	Ngôn ngữ JAVA, giao diện Swing
	CSDL MySQL

- Thư viện


	JRE 1.8
	SDK 1.8
	mysql-connector-java-8.0.27.jar

## Cấu hình

- Chạy server ORB: `orbd -ORBInitialPort 1050 -ORBInitialHost localhost`
- Biên dịch Chat.idl chạy lệnh `-td src -fall Chat.idl`
- Biên dịch và chạy ChatServer với tham số `-ORBInitialPort 1050 -ORBInitialHost localhost`
- Biên dịch và chạy nhiều ChatClient với tham số `-ORBInitialPort 1050 -ORBInitialHost localhost`

## CSDL MySQL
- Tạo Schemas 
```javascript
CREATE DATABASE IF NOT EXISTS orb_chatter;
USE orb_chatter;
```
- Tạo bảng Chatter
```javascript
CREATE TABLE `chatter` (
  `chatterID` varchar(45) NOT NULL,
  `chatterName` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `password_base64` varchar(200) DEFAULT NULL,
  `password_hash` varchar(200) DEFAULT NULL,
  `salt` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`chatterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```
- Tạo bảng Message
```javascript
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chatterID` varchar(100) DEFAULT NULL,
  `receiver` varchar(100) DEFAULT NULL,
  `dateupdate` datetime DEFAULT NULL,
  `messageText` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```
- Diagram

![](https://i.ibb.co/T2q5BR3/CHAT.jpg)
> Relationship 2 table in MySQL

- Feed Data
`insert into orb_chatter.chatter(chatterID, chatterName, password, password_hash, salt, password_base64) values(?,?,?,?,?,?)`

## FlowChart

```flow
st=>start: ĐĂNG NHẬP
op=>operation: KIỂM TRA TÀI KHOẢN
cond=>condition: Successful Yes or No?
e=>end: Người dùng + Token
sd=>operation: SỬ DỤNG HỆ THỐNG

st->op->cond
cond(yes)->e
cond(no)->op
e->sd
```

## Sequence Diagram

```seq
ChatClient->ChatServer: Request Login 
Note right of ChatClient: username and password
ChatServer->MySQL: Correct user? 
MySQL-->ChatServer: Info Response
ChatServer-->ChatClient: Info User and Token
ChatClient->ChatServer: Call Functions
ChatServer-->ChatClient: Check token and Response
```

## Các chức năng của ứng dụng Chat

```
Tạo room chat
Join room chat
Rời room chat
Đổi tên hiển thị trong room
Gửi tin nhắn trong room 
Hiện lịch sử đã chat khi join vào room
```

![](https://i.ibb.co/rwwcGYG/Capture.jpg)
