package mySQL;

import java.io.Serializable;
import java.time.LocalDateTime;

public class MessageSQL implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String chatterID;
	private String receiver;
	private LocalDateTime dateupdate ;
	private String messageText;
	public MessageSQL(int id, String chatterID, String receiver, LocalDateTime dateupdate, String messageText) {
		super();
		this.id = id;
		this.chatterID = chatterID;
		this.receiver = receiver;
		this.dateupdate = dateupdate;
		this.messageText = messageText;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getChatterID() {
		return chatterID;
	}
	public void setChatterID(String chatterID) {
		this.chatterID = chatterID;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public LocalDateTime getDateupdate() {
		return dateupdate;
	}
	public void setDateupdate(LocalDateTime dateupdate) {
		this.dateupdate = dateupdate;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", chatterID=" + chatterID + ", receiver=" + receiver + ", dateupdate="
				+ dateupdate + ", messageText=" + messageText + "]";
	}
	
	
	
}
