package mySQL;

import java.sql.*;


public class MySQLAdapter {

	public static ResultSet runQuery(String sqlQuery,String dbname) throws Exception {
		//String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		String DB_URL = "jdbc:mysql://localhost:3306/" + dbname;

		String USER = "root";
		String PASS = "mtq311";

		Connection conn = null;
		Statement stmt = null;

		Class.forName(JDBC_DRIVER);

		System.out.println("Connecting to a selectd database ...");
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		System.out.println("Connected database successfully...");  

		//Execute a query 
		System.out.println("Creating statement..."); 
		System.out.println("runQuery: " + sqlQuery);

		stmt = conn.createStatement();  
		ResultSet rs = stmt.executeQuery(sqlQuery);  
		return rs;
		
	}
	
	public static int runUpdate(String sqlQuery, String dbname) throws Exception {
		//String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		String DB_URL = "jdbc:mysql://localhost:3306/" +  dbname;

		String USER = "root";
		String PASS = "mtq311";

		Connection conn = null;
		Statement stmt = null;

		Class.forName(JDBC_DRIVER);

		System.out.println("Connecting to a selectd database ...");
		conn = DriverManager.getConnection(DB_URL,USER,PASS);
		System.out.println("Connected database successfully...");  

		//Execute a query 
		System.out.println("Creating statement..."); 
		System.out.println("runUpdate: " + sqlQuery);

		stmt = conn.createStatement();  
		return  stmt.executeUpdate(sqlQuery);  
	}
}
