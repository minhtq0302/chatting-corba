package mySQL;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import chat.MessageHistory;


public class ChatterAdapter {
	private ChatterDAL chatter;
	private String msql = "";
	private String dbname = "orb_chatter";
	private String tbname = "chatter";
	public ChatterAdapter(ChatterDAL ct) throws Exception {
		this.chatter = ct;
//		autoGeneralDatabaseWithName("");
//		autoGeneralTableWithName(tbname);
//		autoFeedData(tbname);
	}

	public boolean checkClientCredintials() throws Exception {
		msql = "select * from " + dbname+"."+tbname + " where chatterID = '" + chatter.getUsername() + "'" ;
		ResultSet rs = MySQLAdapter.runQuery(msql, dbname);
		if(rs != null) {
			rs.next();
			try {
//				String usrID = rs.getString("chatterID");
				String usr = rs.getString("chatterName");
				String pws = rs.getString("password");
//				String pws_base64 = rs.getString("password_base64");
				String pws_hash = rs.getString("password_hash");
				String salt = rs.getString("salt");
				ChatterDAL chatterdb = new ChatterDAL(usr, pws, pws_hash, chatter.getPassword_base64(), salt);

				byte[] byte_chatter_pws_hash = PBKDF2.getEncryptedPassword(chatter.getPassword_base64(), salt.getBytes());

				String chatter_pws_hash = PBKDF2.bytesToHex(byte_chatter_pws_hash);
				chatter.setPassword_PBKDF2(chatter_pws_hash);
				if(chatterdb.getPassword_PBKDF2().equalsIgnoreCase(chatter.getPassword_PBKDF2())) {
					// general new salt
					byte[] salt_byte =  PBKDF2.generateSalt();
					salt = PBKDF2.bytesToHex(salt_byte);
					salt_byte = salt.getBytes();

					// general new password_hash
					byte_chatter_pws_hash = PBKDF2.getEncryptedPassword(chatter.getPassword_base64(), salt_byte);
					chatter_pws_hash = PBKDF2.bytesToHex(byte_chatter_pws_hash);
					chatterdb.setPassword_PBKDF2(chatter_pws_hash);
					chatterdb.setSalt(salt);

					//update password_hash and salt
					msql = "update " +dbname + "." + tbname + " set password_hash = '" +chatterdb.getPassword_PBKDF2()+ "', "
							+ "salt = '" + chatterdb.getSalt() + "' "
							+ "where chatterID = '" + chatterdb.getUsername() + "'";
					int result = MySQLAdapter.runUpdate(msql, dbname);

					return result > 0 ;
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		return false;

	}

	public void autoGeneralDatabaseWithName(String nameDB) throws Exception {
		msql = "CREATE DATABASE IF NOT EXISTS "+dbname+"; ";
		//+ "USE "+dbname+"; ";
		MySQLAdapter.runUpdate(msql, nameDB);
	}

	public void autoGeneralTableWithName(String nameTB) throws Exception {
		msql = "create table if not exists " + nameTB + "( " 
				+ "chatterID varchar(45) PRIMARY KEY, "
				+ "chatterName varchar(45), "
				+ "password varchar(45), "
				+ "password_base64 varchar(200), "
				+ "password_hash varchar(200), " 
				+ "salt varchar(200) "
				+ ");";
		MySQLAdapter.runUpdate(msql, dbname);
	}

	public void autoFeedData(String nameTB) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String[] chs = new String[] {"QMINH", "MUOC", "MNGUYEN", "VTHANH", "NTAI"};

		for(int i = 0; i < chs.length; i++) 
		{
			String chatterID = chs[i];
			String chatterName = chatterID;
			String pws = "123"; // default password 
			String password = Base64.encode(pws.getBytes());
			byte[] byte_salt = PBKDF2.generateSalt();
			String salt = PBKDF2.bytesToHex(byte_salt);
			byte_salt = salt.getBytes();

			byte[] byte_password_hash = PBKDF2.getEncryptedPassword(password, byte_salt);
			String password_hash = PBKDF2.bytesToHex(byte_password_hash);

			msql = "insert into "+ dbname + "." + nameTB + 
					"(chatterID, chatterName, password, password_hash, salt, password_base64) " +
					"values('"+ chatterID +"','"+chatterName+"','"+ pws +"','"+password_hash+"','"+salt+"','"+password+"')";
			try {
				MySQLAdapter.runUpdate(msql, dbname);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("chatterID " + chatterID + " exists !");
			}
		}
	}
	
	public void insertMessage(MessageSQL msg) {
		msql = "insert into message(chatterID, receiver, dateupdate, messageText) "+
			   "values('"+msg.getChatterID()+"','"+msg.getReceiver()+"','"+msg.getDateupdate()+"','"+msg.getMessageText()+"')";
		try {
			MySQLAdapter.runUpdate(msql, dbname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public MessageHistory[] getMessagesHistory(String username) throws Exception{
		msql = "select * from message where( receiver = 'ALL' "
				+ "or chatterID ='"+username+"' or receiver = '"+username+"') "
				+ "and Left(messageText, 8) <> '[Server]' "
				+ "order by dateupdate";
		ResultSet result = MySQLAdapter.runQuery(msql, dbname);
		if(result != null) {
			List<MessageHistory> lst = new ArrayList<MessageHistory>();
			while(result.next())
			{
				int id = result.getInt("id");
				String chatterID = result.getString("chatterID");
				String receiver = result.getString("receiver");
				LocalDateTime dateupdate = result.getTimestamp("dateupdate").toLocalDateTime();
				String messageText = result.getString("messageText");
				
				MessageHistory msg = new MessageHistory(id, chatterID, receiver, dateupdate.toString(), messageText);
				lst.add(msg);
			}
			MessageHistory[] arr = new MessageHistory[lst.size()];
			for(int i = 0 ; i< lst.size(); i++) {
				arr[i] = lst.get(i);
			}
			return arr;
		}
		return null;
	}

	public ChatterDAL getChatter() {
		return chatter;
	}

	public void setChatter(ChatterDAL chatter) {
		this.chatter = chatter;
	}

	public String getMsql() {
		return msql;
	}

	public void setMsql(String msql) {
		this.msql = msql;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getTbname() {
		return tbname;
	}

	public void setTbname(String tbname) {
		this.tbname = tbname;
	}

	

}
