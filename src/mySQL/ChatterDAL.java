package mySQL;

public class ChatterDAL {
	private String username;
	private String password;
	private String password_PBKDF2;
	private String password_base64;
	private String salt;
	public ChatterDAL(String username, String password, String password_PBKDF2, String password_base64, String salt) {
		super();
		this.username = username;
		this.password = password;
		this.password_PBKDF2 = password_PBKDF2;
		this.password_base64 = password_base64;
		this.salt = salt;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword_PBKDF2() {
		return password_PBKDF2;
	}
	public void setPassword_PBKDF2(String password_PBKDF2) {
		this.password_PBKDF2 = password_PBKDF2;
	}
	public String getPassword_base64() {
		return password_base64;
	}
	public void setPassword_base64(String password_base64) {
		this.password_base64 = password_base64;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	@Override
	public String toString() {
		return "Chatter [username=" + username + ", password=" + password + ", password_PBKDF2=" + password_PBKDF2
				+ ", password_base64=" + password_base64 + ", salt=" + salt + "]";
	}
	
	
}
